﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Globalization;
using System.Windows.Media.Animation;



namespace DibujandoFormas
{
    public class Triangulo : FiguraAbstracta
    {
        public Triangulo()
        {
            
            CalculateGeometry();
        }

        public new String name
        {
            get { return String.Format("Triángulo   "); }
        }

        public override string ToString()
        {

            return "Triángulo";
        }


        public override void CalculateGeometry()
        {
            Rect r = new Rect(-anchoAbstracto / 2, -altoAbstracto / 2, anchoAbstracto, altoAbstracto);
            rectanguloPosicion = r;

            PathFigure pf = new PathFigure();
            pf.StartPoint = new Point(-anchoAbstracto / 2, altoAbstracto / 2);
            pf.IsClosed = true;
            pf.Segments.Add(new LineSegment(new Point(anchoAbstracto / 2, altoAbstracto / 2), true));
            pf.Segments.Add(new LineSegment(new Point(0, -altoAbstracto / 2), true));


            PathGeometry pg = new PathGeometry();
            pg.Figures.Add(pf);

            bezierPath.Data = pg;

            DrawingContext dc = RenderOpen();

            dc.DrawGeometry(brochaRelleno, penAbstracto, pg);
            //dc.DrawRectangle(null, new Pen(Brushes.Red, 1), LienzoDibujo);
            dc.Close();

            base.CalculateGeometry();
        }



        public override void DrawContour(DrawingContext dc, Pen pen, double xoffset = 0, double yoffset = 0, double rot = 0, double scalex = 1, double scaley = 1)
        {
            Point pt = new Point(posicion.X + xoffset, posicion.Y + yoffset);
            ScaleTransform stf = new ScaleTransform(scalex, scaley, pt.X, pt.Y);
            dc.PushTransform(stf);
            RotateTransform rtf = new RotateTransform(anguloAbstracto + rot, pt.X, pt.Y);
            dc.PushTransform(rtf);
            TranslateTransform ttf = new TranslateTransform(pt.X, pt.Y);
            dc.PushTransform(ttf);

            dc.DrawGeometry(null, pen, bezierPath.Data);
            dc.Pop();
            dc.Pop();
            dc.Pop();
        }
  
    }
}