﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Globalization;
using System.Windows.Media.Animation;



namespace DibujandoFormas
{

    public partial class Contenedor : FrameworkElement, INotifyPropertyChanged
    {

        private ObservableCollection<FiguraAbstracta> coleccionObservableDeFiguras;
        private VisualCollection coleccionDeFiguras;
        private DispatcherTimer refresco;
        private Rect lienzoDibujoRect;
        public Pen pen = new Pen(Brushes.Red, 6);

        public Rect cuadradoLienzo
        {
            get { return lienzoDibujoRect; }
        }

        public ObservableCollection<FiguraAbstracta> tablaFigurasXAML
        {
            get { return coleccionObservableDeFiguras; }
        }

        public DispatcherTimer timer
        {
            get { return refresco; }
        }


        // INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public Contenedor()
        {
            coleccionDeFiguras = new VisualCollection(this);
            lienzoDibujoRect = new Rect(0, 0, ActualWidth, ActualHeight);

            lienzoDibujoRect = new Rect(0, 0, ActualWidth, ActualHeight);


            coleccionObservableDeFiguras = new ObservableCollection<FiguraAbstracta>();


            // Temporizador de eventos ocurridos en el LIENZO//
            refresco = new DispatcherTimer();
            timer.Tick += OnTick;
            refresco.Interval = new TimeSpan(0, 0, 0, 0, 1);


        }



        // https://code.msdn.microsoft.com/windowsapps/Utilizando-Timer-en-WPF-38c44eb6
        private void OnTick(object sender, EventArgs e)
        {
          

            if (coleccionDeFiguras == null) return;
            Controladora controladora = ((MainWindow)Application.Current.MainWindow).mixamlcontroladora;
            if (controladora != null)
            {
                controladora.InvalidateVisual();

            }

        }

        public void OnWindowResized()
        {
            lienzoDibujoRect.Width = ActualWidth;
            lienzoDibujoRect.Height = ActualHeight;
            InvalidateVisual();
        }




        public bool agregaFigura(FiguraAbstracta shape)
        {
            if (shape == null)
            {
                return false;
            }
            coleccionDeFiguras.Add(shape);
            coleccionObservableDeFiguras.Add(shape);

            return true;
        }
        public bool eliminaFigura(FiguraAbstracta shape)
        {
            if (shape == null) return false;
            coleccionDeFiguras.Remove(shape);
            coleccionObservableDeFiguras.Remove(shape);
            return true;
        }

        public bool limpiarLienzo()
        {

            coleccionDeFiguras.Clear();
            coleccionObservableDeFiguras.Clear();
            return true;

        }

        // Provide a required override for the VisualChildrenCount property. 
        protected override int VisualChildrenCount
        {
            get { return coleccionDeFiguras.Count; }
        }

        // Provide a required override for the GetVisualChild method. 
        protected override Visual GetVisualChild(int index)
        {
            if (index < 0 || index >= coleccionDeFiguras.Count)
            {
                throw new ArgumentOutOfRangeException();
            }
            // Debug.Write("mahuena");
            return coleccionDeFiguras[index];
        }






        public FiguraAbstracta comprobarFiguraLienzoSeleccionada(Point pt)
        {

            HitTestResult result = VisualTreeHelper.HitTest(this, pt);
            FiguraAbstracta figuraEnLienzo = null;
            
            if (result.VisualHit.GetType().BaseType == typeof(FiguraAbstracta))
            {
                figuraEnLienzo = (FiguraAbstracta)result.VisualHit;
            }

            return figuraEnLienzo;
        }

        protected override void OnRender(DrawingContext dc)
        {
            Color c = Color.FromArgb(255, 0, 51, 51);

            dc.DrawRectangle(new SolidColorBrush(c), null, lienzoDibujoRect);
            pen = new Pen(Brushes.LightBlue, 1);
            Point pinicio = new Point();
            Point pfin = new Point();

            int i;
            int j = 50;
            for (i = 1; i < 60; i++)
            {

                pinicio.X = 0;
                pinicio.Y = 50 * i;

                pfin.X = 10000;
                pfin.Y = 50 * i;

                dc.DrawLine(pen, pinicio, pfin);
            }


            for (i = 1; i < 60; i++)
            {

                pinicio.X = 50 * i;
                pinicio.Y = 0;

                pfin.X = 50 * i;
                pfin.Y = 10000;

                dc.DrawLine(pen, pinicio, pfin);
            }









        }
    }
}