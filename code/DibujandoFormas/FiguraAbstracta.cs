﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Globalization;
using System.Windows.Media.Animation;



namespace DibujandoFormas
{


    public class FiguraAbstracta : DrawingVisual, INotifyPropertyChanged
    {

       

        protected Vector posicion;
        protected Point origen;
        protected Point fin;
        protected double altoAbstracto;
        protected double anchoAbstracto;  
        protected double anguloAbstracto;

        private static uint registroCivilFiguras = 0;
        private uint figuraNumero;
        protected Rect rectanguloPosicion;
        protected Brush brochaRelleno;
        protected Pen penAbstracto;
        protected double grosorAbs;        
        protected Path bezierPath;     

        
        
        
        
        // INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        //Default Constructor
        public FiguraAbstracta()
        {
            figuraNumero = registroCivilFiguras++;

        
            _scalex = _scaley = 1;

            origen.X = 150;
            origen.Y = 150;

            fin.X = 300;
            fin.Y = 300;

            posicion.X = 150;
            posicion.Y = 150;
            anguloAbstracto = 0;

            altoAbstracto = 100;
            anchoAbstracto = 100;


            grosorAbs = 10;
            Color color = Color.FromRgb(255, 183, 0);
            brochaRelleno = new SolidColorBrush(color);
            Color color2 = Color.FromRgb(0, 0, 0);
            penAbstracto = new Pen(new SolidColorBrush(color2), grosorAbs);
            bezierPath = new Path();

        }

           public void  normalizar()
        {


          
            _scalex = _scaley = 1;

         


            origen.X = 150;
            origen.Y = 150;

            fin.X = 300;
            fin.Y = 300;
            posicion.X = 150;
            posicion.Y = 150;
            anguloAbstracto = 0;

            altoAbstracto = 100;
            anchoAbstracto = 100;


            grosorAbs = 10;
            Color color = Color.FromRgb(255, 183, 0);
            brochaRelleno = new SolidColorBrush(color);
            Color color2 = Color.FromRgb(0, 0, 0);
            penAbstracto = new Pen(new SolidColorBrush(color2), grosorAbs);
            bezierPath = new Path();
        }

        //Geometry calculation
        public virtual void CalculateGeometry()
        {

            Update();
        }

        //Update function
        public virtual void Update()
        {
            RotateTransform rtf = new RotateTransform(anguloAbstracto);
            ScaleTransform stf = new ScaleTransform(_scalex, _scaley);

            TransformGroup tfg = new TransformGroup();
            tfg.Children.Add(stf);
            tfg.Children.Add(rtf);

            this.Transform = tfg;
            this.Offset = posicion;

        }





       

        /********************************************/

        /** Public properties ***********************/
        public Point propOrigen
        {
            get { return origen; }
            set
            {
                origen = value;
                OnPropertyChanged("propOrigen");
                Update();
            }
        }


        public Point propfin
        {
            get { return fin; }
            set
            {
                fin = value;
                OnPropertyChanged("propfin");
                Update();
            }
        }


        public double propOrigenX
        {
            get { return origen.X; }
            set { origen.X = value;
           
            OnPropertyChanged("propOrigenX");
            Update();
            CalculateGeometry();
      
            }
        }


        public double propOrigenY
        {
            get { return origen.Y; }
            set { origen.Y = value;
            Debug.WriteLine("sk pY");
            OnPropertyChanged("propOrigenY");
            Update();
            CalculateGeometry();
            }
        }


        public double propFinX
        {
            get { return fin.X; }
            set { fin.X = value;
            Debug.WriteLine("crap pX");
            OnPropertyChanged("propFinX");
            Update();
            CalculateGeometry();
        

         
            }
        }


        public double propFinY
        {
            get { return fin.Y; }
            set {
                
                fin.Y = value;
            Debug.WriteLine("crap pY");
            OnPropertyChanged("propFinY");
            Update();
            CalculateGeometry();
          
            }
        }

        public Vector valorPosicion
        {
            get { return posicion; }
            set
            {
                posicion = value;
                OnPropertyChanged("pos");
               
            }
        }

        public double valorAngulo
        {
            get { return anguloAbstracto; }
            set
            {
                anguloAbstracto = value;
                OnPropertyChanged("valorAngulo");
                Update();
            }
        }

        public SolidColorBrush rellenoDeBrocha
        {
            get { return (SolidColorBrush)brochaRelleno; }
            set
            {
                brochaRelleno = (Brush)value;
                OnPropertyChanged("rellenoDeBrocha");
            }

        }
        public Pen valorPen
        {
            get { return penAbstracto; }
            set
            {
                penAbstracto = value;
                OnPropertyChanged("valorPen");
            }

        }

        public SolidColorBrush brochaPen
        {
            get { return (SolidColorBrush)(penAbstracto.Brush); }
            set
            {
                penAbstracto.Brush = (Brush)value;
                OnPropertyChanged("valorPen");
            }
        }
        public double valorGrosor
        {
            get { return grosorAbs; }
            set
            {
                grosorAbs = value;
                penAbstracto.Thickness = value;
                OnPropertyChanged("valorGrosor");
            }

        }
        public double alto
        {
            get { return altoAbstracto; }
            set
            {
                altoAbstracto = value;
                OnPropertyChanged("alto");
                CalculateGeometry();
            }

        }




      
        
        public double ancho
        {
            get { return anchoAbstracto; }
            set
            {
                anchoAbstracto = value;
                OnPropertyChanged("ancho");
                CalculateGeometry();
            }

        }
    

        public double pX
        {
            get { return posicion.X; }
            set
            {
               
                posicion.X = value;
                OnPropertyChanged("pX");
                
                Update();
            }
        }
        public double pY 
        {
            get { return posicion.Y; }
            set
            {
                posicion.Y = value;
                OnPropertyChanged("pY");
                OnPropertyChanged("valorPosicion");
                Update();
            }
        }


        

        public double _scalex
        {
            get;
            set;
        }

        public double _scaley
        {
            get;
            set;
        }

        public Rect boundingBox
        {
            get { return rectanguloPosicion; }
        }

        public Path path { get { return bezierPath; } }

        public uint numeroid
        {
            get { return figuraNumero; }
        }

        /********************************************/

        //Static factory
        public static FiguraAbstracta CreateShape(figurasEnnumeradas type, Vector p, Point oRecta, Point fRecta, double width, double height, double phi, SolidColorBrush brush, Pen pen)
        {
            Random rng = new Random();
            if (brush == null)
            {
                Color color = Color.FromRgb(255, 183, 0);
                brush = new SolidColorBrush(color);
                //  brush = Brushes.Gold;
            }
            if (pen == null)
            {

                double strokeThickness = 6;
                Color color2 = Color.FromRgb(0, 0, 0);

                pen = new Pen(new SolidColorBrush(color2), strokeThickness);
            }
            FiguraAbstracta shape = null;
            switch (type)
            {
                case figurasEnnumeradas.Rectangulo:
                    shape = new Rectangulo();
                    break;
                case figurasEnnumeradas.Elipse:
                    shape = new CEllipse();
                    break;
                
                case figurasEnnumeradas.Recta:
                    shape = new MoldeRecta();
                    break;
                case figurasEnnumeradas.RectanguloRed:
                    shape = new RectanguloRedondeado();
                    break;
                case figurasEnnumeradas.Circulo:
                    shape = new Circulo();
                    break;
                  case figurasEnnumeradas.Triangulo:
                      shape = new Triangulo();
                      break;
                case figurasEnnumeradas.Rombo:
                      shape = new Rombo();
                      break;
            }
            shape.valorPosicion = p;
            shape.ancho = width;
            shape.alto = height;
            shape.propOrigen = oRecta;
            shape.propfin = fRecta;
            shape.valorAngulo = phi;

          
            shape.rellenoDeBrocha = brush;
            shape.valorPen = pen;
            shape.valorGrosor = pen.Thickness;

            shape.CalculateGeometry();
            shape.Update();

            return shape;
        }



      

        public virtual void DrawContour(DrawingContext dc, Pen pen, double xoffset = 0, double yoffset = 0, double rot = 0, double scalex = 1, double scaley = 1)
        {
            if (dc == null) return;
        }



       
        public String name
        {
            get { return "Figura ABSTRACTA"; }
        }

    }

}