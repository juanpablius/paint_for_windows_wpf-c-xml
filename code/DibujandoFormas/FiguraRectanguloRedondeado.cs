﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Globalization;
using System.Windows.Media.Animation;



namespace DibujandoFormas
{
    public class RectanguloRedondeado : FiguraAbstracta
    {
        public RectanguloRedondeado()
        {
            CalculateGeometry();
        }

        public new String name
        {
            get { return "Rect Redon"; }
        }

        public override string ToString()
        {

            return "Rectángulo especial";
        }

        public override void CalculateGeometry()
        {

            Rect r = new Rect(-anchoAbstracto / 2, -altoAbstracto / 2, anchoAbstracto, altoAbstracto);
            rectanguloPosicion = r;
            DrawingContext dc = RenderOpen();
           // dc.DrawRectangle(brochaRelleno, penAbstracto, r);
            dc.DrawRoundedRectangle(brochaRelleno, penAbstracto, r, 25, 25);
            dc.Close();

            base.CalculateGeometry();
        }


        public override void DrawContour(DrawingContext dc, Pen pen, double xoffset = 0, double yoffset = 0, double rot = 0, double scalex = 1, double scaley = 1)
        {

            RotateTransform rtf = new RotateTransform(anguloAbstracto + rot, posicion.X + xoffset, posicion.Y + yoffset);
            dc.PushTransform(rtf);
            Rect r = rectanguloPosicion;
            r.Inflate(r.Width * (scalex - 1) / 2, r.Height * (scaley - 1) / 2);
            Vector v = new Vector(posicion.X + xoffset, posicion.Y + yoffset);
            r.Offset(v);
         
            dc.DrawRoundedRectangle(null, pen, r, 25, 25);
            dc.Pop();
        }
    }

}