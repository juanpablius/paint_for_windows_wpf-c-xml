﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Globalization;
using System.Windows.Media.Animation;



namespace DibujandoFormas
{
public class Controladora : Canvas, INotifyPropertyChanged
{
    public event PropertyChangedEventHandler PropertyChanged;
    public void OnPropertyChanged(String propertyName)
    {
        if (PropertyChanged != null)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
    public bool CS = false;
    public bool jp = false;
    public Contenedor _lienzoRef;
    private bool clickIzquierdoActivo; 
    private bool arrancado = true;
    private Point primerPunto;
    private Point puntoFinal;
 

    private botonesEnnumerados radioBoton;
    private figurasEnnumeradas tipoDeFigura;
    private FiguraAbstracta _selectedShape;
    private Pen pen;
    private FiguraAbstracta _randomShape;
    private Rect _boundingBox;



    public Point clickStartPoint
    {
        get { return primerPunto; }
    }
    public Point clickEndPoint
    {
        get { return puntoFinal; }
    }
    public bool lclicked
    {
        get { return clickIzquierdoActivo; }
    }



    public botonesEnnumerados valorRadioBoton
    {
        get { return radioBoton; }
        set
        {
            radioBoton = value;
            OnToolChanged();
            OnPropertyChanged("valorRadioBoton");
        }
    }

    public figurasEnnumeradas valorTipoFigura
    {
        get { return tipoDeFigura; }
        set
        {
            tipoDeFigura = value;
            OnPropertyChanged("valorTipoFigura");
            OnPropertyChanged("figuraSeleccionadaListbox");
        }
    }

    public FiguraAbstracta figuraSeleccionada
    {
        get { return _selectedShape; }
        set
        {
            _selectedShape = value;
            OnPropertyChanged("figuraSeleccionada");
            OnSelectionChanged();
        }
    }


    private void OnToolChanged()
    {
        clickIzquierdoActivo = false;
      //  Select();
        Seleccionar();
        switch (radioBoton)
        {
            case botonesEnnumerados.Tool_Cursor:
                Cursor = Cursors.Arrow;
                break;
            case botonesEnnumerados.Tool_Pencil:
                Cursor = Cursors.Cross;
                break;
            case botonesEnnumerados.Modifica:
                Cursor = Cursors.ScrollAll;
                break;

        }

    }

    private void OnSelectionChanged()
    {
        OnPropertyChanged("anySelected");
    }

    public FiguraAbstracta randomShape
    {
        get { return _randomShape; }
    }

    SolidColorBrush alphaBrush = new SolidColorBrush(Color.FromArgb(7, 7, 7, 7)); //  
    

  

    public FiguraAbstracta figuraSeleccionadaListbox
    {

        get {
            // Si está seleccionada la figura molde, aún no se ha hecho click en la tabla
            if (_selectedShape == _randomShape) return null; 
            //  Del contrario, devolveremos la figura deseada
            else return _selectedShape;
           
        
        }
        set
        {
            if (value == null) { _selectedShape = _randomShape; }
            else _selectedShape = value; // Si el valor es nulo, le asignaremos la figura molde
         
            OnPropertyChanged("figuraSeleccionadaListbox");
            OnPropertyChanged("figuraSeleccionada");
        }
    }

    public Controladora()
    {
        tipoDeFigura = figurasEnnumeradas.Rectangulo;
        radioBoton = botonesEnnumerados.Tool_Pencil;
        pen = new Pen(Brushes.Red, 5);
        _boundingBox = new Rect(0, 0, ActualWidth, ActualHeight);
        _randomShape = new FiguraAbstracta();
        _selectedShape = _randomShape;
    }

    public void OnWindowResized()
    {
        _boundingBox = new Rect(0, 0, ActualWidth, ActualHeight);
    }

    /*EVENTOS DE RATÓN*/


    protected override void OnMouseDown(MouseButtonEventArgs e)
    {

        if (!clickIzquierdoActivo)
        {


            /*Comprobar si se ha clickeado una figura en el lienzo*/
            Point pt = e.GetPosition(this);
            if (e.LeftButton == MouseButtonState.Pressed) clickIzquierdoActivo = true;

            FiguraAbstracta figuraSel = ((MainWindow)Window.GetWindow(this)).mixamllienzo.comprobarFiguraLienzoSeleccionada(pt);

            if (figuraSel != null && radioBoton != botonesEnnumerados.Tool_Pencil)
            {
                // Select(figuraEnLienzo, true);
                Seleccionar(figuraSel);
                Debug.Write("Figura selecccionada sobre lienzo");
            }
            else
            {
                //  Select();
                Seleccionar();
                Debug.Write("Sin figura seleccionada sobre lienzo");
            }





            primerPunto = puntoFinal = pt;

        }

    }

    protected override void OnMouseUp(MouseButtonEventArgs e)
    {
        Point pt = e.GetPosition(this);
        bool left = (e.LeftButton == MouseButtonState.Released);
      

     //   Select(_selectedShape, false);

        Seleccionar(_selectedShape);
        Vector d; 
        Vector c; //clicked point vector


        if (left && clickIzquierdoActivo)
        {
            puntoFinal = pt;
    
            switch (radioBoton)
            {
                case botonesEnnumerados.Tool_Cursor:

                    if (_selectedShape != _randomShape) 
                    {
                        d = new Vector(puntoFinal.X - primerPunto.X, puntoFinal.Y - primerPunto.Y); 
                        Vector p = _selectedShape.valorPosicion;
                        _selectedShape.valorPosicion = p + d;
                        _selectedShape.Update();


                        /* La recta se mide por un punto incial y otro final*/
                        if ("Recta".Equals(_selectedShape.ToString()))
                        {
                            String oh4 = String.Format("Recta -ORIGEN X: {0} Y: {1} - DESTINO X: {2:0.000}  Y: {3:0.000}", _selectedShape.propOrigenX, _selectedShape.propOrigenY, _selectedShape.propFinX, _selectedShape.propFinY);
                            ((MainWindow)Application.Current.MainWindow).labelSI.Content = oh4;
                        }

                        /* Las demás figuras por un origen , un alto, un ancho y una inclinación */
                        else
                        {
                            String oh4 = String.Format("{4} - X: {0} Y: {1} - Ancho: {2:0.00} - Alto: {3:0.00} - Inclinación : {5:0.0}", _selectedShape.pX, _selectedShape.pY, _selectedShape.ancho, _selectedShape.alto, _selectedShape.ToString(), _selectedShape.valorAngulo);
                            ((MainWindow)Application.Current.MainWindow).labelSI.Content = oh4;
                        }
                    }
                    break;


                case botonesEnnumerados.Tool_Pencil:
                    
                    Rect rect = new Rect(clickStartPoint, clickEndPoint);
                    Point po = new Point();
                    Point pf = new Point();

                    Vector v = new Vector(rect.Left + rect.Width / 2, rect.Top + rect.Height / 2); // Origen CENTRAL de la figura

                    po = clickStartPoint;
                    pf = clickEndPoint;
                    
                    FiguraAbstracta newshape = FiguraAbstracta.CreateShape(tipoDeFigura, v, po, pf, rect.Width, rect.Height, 0, null, null);
                    if (newshape != null) _lienzoRef.agregaFigura(newshape); 
/*-------------------------------------------------------------*/


                    if (valorTipoFigura == figurasEnnumeradas.Rectangulo)
                    {

                        String oh2 = String.Format("Rectángulo - X: {0} Y: {1} - Ancho: {2:0.000}  Alto: {3:0.000}", v.X, v.Y,  rect.Width  , rect.Height);
                        ((MainWindow)Application.Current.MainWindow).labelSI.Content = oh2;
                    }


                    else if (valorTipoFigura == figurasEnnumeradas.Recta)
                    {

                        String oh2 = String.Format("Recta -ORIGEN X: {0} Y: {1} - DESTINO X: {2:0.000}  Y: {3:0.000}",po.X, po.Y,pf.X,pf.Y);
                        ((MainWindow)Application.Current.MainWindow).labelSI.Content = oh2;
                    }

                    else if (valorTipoFigura == figurasEnnumeradas.Elipse)
                    {

                        String oh2 = String.Format("Elipse - X: {0} Y: {1} - Ancho: {2:0.000}  Alto: {3:0.000}", v.X, v.Y, rect.Width, rect.Height);
                        ((MainWindow)Application.Current.MainWindow).labelSI.Content = oh2;
                    }

                    else if (valorTipoFigura == figurasEnnumeradas.Triangulo)
                    {

                        String oh2 = String.Format("Triágulo - X: {0} Y: {1} - Ancho: {2:0.000}  Alto: {3:0.000}", v.X, v.Y, rect.Width, rect.Height);
                        ((MainWindow)Application.Current.MainWindow).labelSI.Content = oh2;
                    }

                    else if (valorTipoFigura == figurasEnnumeradas.Circulo)
                    {
                        String oh2 = String.Format("Circulo - X: {0} Y: {1} - Ancho: {2:0.000}  Alto: {3:0.000}", v.X, v.Y, rect.Width, rect.Height);
                        ((MainWindow)Application.Current.MainWindow).labelSI.Content = oh2;

                    }

                    else if (valorTipoFigura == figurasEnnumeradas.RectanguloRed)
                    {
                        String oh2 = String.Format("Rectángulo esquinas redondeadas - X: {0} Y: {1} - Ancho: {2:0.000}  Alto: {3:0.000}", v.X, v.Y, rect.Width, rect.Height);
                        ((MainWindow)Application.Current.MainWindow).labelSI.Content = oh2;

                    }

                    else if (valorTipoFigura == figurasEnnumeradas.Rombo)
                    {
                        String oh2 = String.Format("Rombo - X: {0} Y: {1} - Ancho: {2:0.000}  Alto: {3:0.000}", v.X, v.Y, rect.Width, rect.Height);
                        ((MainWindow)Application.Current.MainWindow).labelSI.Content = oh2;

                    }
                    
                    break;

                case botonesEnnumerados.Modifica:

                    //https://escarbandocodigo.wordpress.com/2009/10/06/jugando-con-imagenes-en-wpf-%E2%80%93-parte-2-%E2%80%93-zoom-rotacion-y-traslacion*/
                   


                      if (_selectedShape == _randomShape) break;
                    c = new Vector(primerPunto.X, primerPunto.Y); //Vector to clicked point
                    c -= _selectedShape.valorPosicion;
                    d = new Vector(puntoFinal.X - primerPunto.X, puntoFinal.Y - primerPunto.Y); //dragvector
                    Vector cp = c + d;

                    
                    double angle = (Math.Atan2(c.Y, c.X) - Math.Atan2(cp.Y, cp.X)) / 6.28 * 360;
                    double scale = cp.Length / c.Length;
                 

                    Debug.Write(_selectedShape.ToString());
                    if ("Recta".Equals(_selectedShape.ToString()))
                    {

                      //  _selectedShape.propOrigenX *= scale;
                      //  _selectedShape.propOrigenY *= scale;

                        _selectedShape.propFinX *= scale;
                        _selectedShape.propFinY *= scale;
                        
                        _selectedShape.CalculateGeometry();
                        _selectedShape.Update();

                        String oh3 = String.Format("{4} - X: {0} Y: {1} - Ancho: {2:0.00} - Alto: {3:0.00} - Inclinación {5:0.0}", _selectedShape.pX, _selectedShape.pY, (float)_selectedShape.ancho, _selectedShape.alto, _selectedShape.ToString(), _selectedShape.valorAngulo);
                        ((MainWindow)Application.Current.MainWindow).labelSI.Content = oh3;
                    }

                    else
                    {
                        _selectedShape.ancho *= scale;
                        _selectedShape.alto *= scale;
                        _selectedShape.valorAngulo -= angle;
                        _selectedShape.CalculateGeometry();
                        _selectedShape.Update();
                        String oh3 = String.Format("{4} - X: {0} Y: {1} - Ancho: {2:0.00} - Alto: {3:0.00} - Inclinación {5:0.0}", _selectedShape.pX, _selectedShape.pY, (float)_selectedShape.ancho, _selectedShape.alto, _selectedShape.ToString(), _selectedShape.valorAngulo);
                        ((MainWindow)Application.Current.MainWindow).labelSI.Content = oh3;

                    }




                    break;

            }
            clickIzquierdoActivo = false;


        }
        
    }

    protected override void OnMouseMove(MouseEventArgs e)
    {
        if (clickIzquierdoActivo)
        {
            puntoFinal = e.GetPosition(this);

        }
    }

    protected override void OnMouseLeave(MouseEventArgs e)
    {
        Cursor = Cursors.Arrow;
        if (!(clickIzquierdoActivo)) return;
        clickIzquierdoActivo  = false;
        switch (radioBoton)
        {
            case botonesEnnumerados.Tool_Cursor:
                Debug.Write("aqui");

                //Select(_selectedShape, false);
                Seleccionar(_selectedShape);
                break;

            case botonesEnnumerados.Tool_Pencil:
                //Select();
                Seleccionar();
                break;
        }
       // Select(_selectedShape, false);
        Seleccionar(_selectedShape);
    }

    protected override void OnMouseEnter(MouseEventArgs e)
    {
        switch (radioBoton)
        {
            case botonesEnnumerados.Tool_Cursor:
                Cursor = Cursors.Arrow;
                break;
            case botonesEnnumerados.Tool_Pencil:
                Cursor = Cursors.Cross;
                break;
            case botonesEnnumerados.Modifica:
                Cursor = Cursors.ScrollAll;
                break;
        }
    }

  /***/

    protected override void OnRender(DrawingContext dc)
    {



        dc.DrawRectangle(alphaBrush, null, _boundingBox); // Dibuja el rectángulo de la capa superior, asociado a la controladora
        int k = 40;
        pen = new Pen(Brushes.Red, 1);
        Point pinicio = new Point();
        Point pfin = new Point();
       
        int i;

        /*---Dibujado de las coordenadas numéricas, por encima del lienzo---*/
      
        if (arrancado)
        {
            for (i = 1; i < 60; i++)
            {

                pinicio.X = 5;
                pinicio.Y = 50 * i;

                k = 50 * i;
                string testString = k.ToString();

                FormattedText formattedText = new FormattedText(
                                   testString,
                                   CultureInfo.GetCultureInfo("en-us"),
                                   FlowDirection.LeftToRight,
                                   new Typeface("Verdana"),
                                   8,
                                   Brushes.White);


                dc.DrawText(formattedText, pinicio);
                //   dc.DrawLine(pen, pinicio, pfin);
            }


            for (i = 1; i < 60; i++)
            {

                pinicio.X = 50 * i;
                pinicio.Y = 0;

                k = 50 * i;
                string testString = k.ToString();

                FormattedText formattedText = new FormattedText(
                                   testString,
                                   CultureInfo.GetCultureInfo("en-us"),
                                   FlowDirection.LeftToRight,
                                   new Typeface("Verdana"),
                                   8,
                                   Brushes.White);


                dc.DrawText(formattedText, pinicio);
              
                //  dc.DrawLine(pen, pinicio, pfin);
              
               
            }
        }


        /*--------------------------------------*/

        if (_lienzoRef == null) return;
        FiguraAbstracta shape = _selectedShape;



        Rect r = shape.boundingBox; // Rectángulo contenedor de la figura abstracta

        // figuraEnLienzo.DrawContour(dc, pen);

        switch (radioBoton)
        {
            case botonesEnnumerados.Tool_Cursor:
                if (shape == _randomShape || shape == null)
                {
                    String cadena2 = "Click izquierdo sobre la figura para mover";
                    ((MainWindow)Application.Current.MainWindow).labelSI.Content = cadena2;
                   
                    return;

                }
                if (clickIzquierdoActivo)
                {

                    Vector v = new Vector(clickEndPoint.X - clickStartPoint.X, clickEndPoint.Y - clickStartPoint.Y);
                    Vector p = shape.valorPosicion + v;
                  
                    shape.DrawContour(dc, new Pen(Brushes.Red, 5), v.X, v.Y);

                    String oh = String.Format("{4} - X: {0} Y: {1} - Ancho: {2:0.000} - Alto: {3:0.000} - Inclinación: {5:0.0}", _selectedShape.pX, _selectedShape.pY, (float) _selectedShape.ancho, _selectedShape.alto, _selectedShape.ToString(), _selectedShape.valorAngulo  );
                    ((MainWindow)Application.Current.MainWindow).labelSI.Content = oh;


                    pen = new Pen(Brushes.Aquamarine, 1);



                    int j = 50;
                    for (i = 1; i < 60; i++)
                    {

                        pinicio.X = 0;
                        pinicio.Y = 50 * i;

                        pfin.X = 10000;
                        pfin.Y = 50 * i;

                        dc.DrawLine(pen, pinicio, pfin);
                    }


                    for (i = 1; i < 60; i++)
                    {

                        pinicio.X = 50 * i;
                        pinicio.Y = 0;

                        pfin.X = 50 * i;
                        pfin.Y = 10000;

                        dc.DrawLine(pen, pinicio, pfin);
                    }


                }
                
           
              
                break;
                
            case botonesEnnumerados.Modifica:

                
                if (clickIzquierdoActivo)
                {
                    Vector c2 = new Vector(primerPunto.X, primerPunto.Y); //Vector to clicked point
                    c2 -= shape.valorPosicion;
                    Vector d2 = new Vector(puntoFinal.X - primerPunto.X, puntoFinal.Y - primerPunto.Y); //dragvector
                    Vector cp2 = c2 + d2;

                    double angle2 = (Math.Atan2(c2.Y, c2.X) - Math.Atan2(cp2.Y, cp2.X)) / 6.28 * 360;
                    double scale2 = cp2.Length / c2.Length;

                    shape.DrawContour(dc, pen, 0, 0, -angle2, scale2, scale2);


/* ---------------Rejilla de coordenadas auxiliar-------------------*/
                    pen = new Pen(Brushes.Aqua, 1);
                    int j = 50;
                    for (i = 1; i < 60; i++)
                    {

                        pinicio.X = 0;
                        pinicio.Y = 50 * i;

                        pfin.X = 10000;
                        pfin.Y = 50 * i;

                        dc.DrawLine(pen, pinicio, pfin);
                    }


                    for (i = 1; i < 60; i++)
                    {

                        pinicio.X = 50 * i;
                        pinicio.Y = 0;

                        pfin.X = 50 * i;
                        pfin.Y = 10000;

                        dc.DrawLine(pen, pinicio, pfin);
                    }



                }

/* ---------------Rejilla de coordenadas auxiliar-------------------*/
                break;


            case botonesEnnumerados.Tool_Pencil:

                if (clickIzquierdoActivo)
                {
                    
                    Rect rect = new Rect(clickStartPoint, clickEndPoint);
                   // dc.DrawRectangle(null, pen, rect);
                    Debug.Write(valorTipoFigura);
                    if (valorTipoFigura == figurasEnnumeradas.Rectangulo )
                    {
                       dc.DrawRectangle(null, pen, rect);
                    }

                    else if (valorTipoFigura == figurasEnnumeradas.Elipse)
                    {
                        
                        Point v = new Point(rect.Left + rect.Width / 2, rect.Top + rect.Height / 2);

                        dc.DrawEllipse(null, pen, v, rect.Width /2 , rect.Height /2 );
                    }


                    else if (tipoDeFigura == figurasEnnumeradas.Recta)
                    {
                        dc.DrawLine(pen, clickStartPoint, clickEndPoint);
                    }

                    else if (tipoDeFigura == figurasEnnumeradas.Circulo){

                        Point v = new Point(rect.Left + rect.Width / 2, rect.Top + rect.Height / 2);

                        dc.DrawEllipse(null, pen, v, rect.Width / 2, rect.Width / 2);
                    }

                    else if (tipoDeFigura == figurasEnnumeradas.RectanguloRed)
                    {
                        dc.DrawRoundedRectangle(null, pen, rect, 25, 25);
                    }

                    else if (tipoDeFigura == figurasEnnumeradas.Triangulo)
                    {
                        dc.DrawRectangle(null, pen, rect);
                         

                    }

                    else if (tipoDeFigura == figurasEnnumeradas.Rombo)
                    {
                        dc.DrawRectangle(null, pen, rect);
                    }
                    Color c = Color.FromArgb(255, 0, 51, 51);

/* ---------------Rejilla de coordenadas auxiliar-------------------*/
                  
                    pen = new Pen(Brushes.Aquamarine, 1);      
                    int j = 50;
                    for (i = 1; i < 60; i++)
                    {

                        pinicio.X = 0;
                        pinicio.Y = 50 * i;

                        pfin.X = 10000;
                        pfin.Y = 50 * i;

                        dc.DrawLine(pen, pinicio, pfin);
                    }


                    for (i = 1; i < 60; i++)
                    {

                        pinicio.X = 50 * i;
                        pinicio.Y = 0;

                        pfin.X = 50 * i;
                        pfin.Y = 10000;

                        dc.DrawLine(pen, pinicio, pfin);
                    }
/* ---------------Rejilla de coordenadas auxiliar-------------------*/
          


                }
              


                break;
        
            case botonesEnnumerados.Goma:

                 String gom = String.Format("Click sobre la figura para borrar");
                    ((MainWindow)Application.Current.MainWindow).labelSI.Content = gom;
                if (shape != null && radioBoton == botonesEnnumerados.Goma && clickIzquierdoActivo)
                {
                    Debug.Write("jaja");
                    //Select(figuraEnLienzo);
                    Seleccionar(shape);
                    RemoveShape();
                    //return;
                }
                break;
        }



    }


    public void Seleccionar(FiguraAbstracta shape = null)
    {

        if (shape == _selectedShape) return;

        if (shape == null) figuraSeleccionada = _randomShape;

        else
        {
            figuraSeleccionada = shape;


            String oh = String.Format("{4} - X: {0} Y: {1} - Ancho: {2:0.000} - Alto: {3:0.000} - Inclinación: {5:0.0}", shape.pX, shape.pY, shape.ancho, shape.alto, shape.ToString(), shape.valorAngulo);
            ((MainWindow)Application.Current.MainWindow).labelSI.Content = oh;

        }


        figuraSeleccionadaListbox = shape;
    }

    
  
    public void RemoveShape()
    {
        if (_selectedShape != null) _lienzoRef.eliminaFigura(_selectedShape);
        
  
        Seleccionar();
    }


    public void OrdenaLimpiarLienzo()
    {
        _lienzoRef.limpiarLienzo();
 
        Seleccionar();
    }
}
}