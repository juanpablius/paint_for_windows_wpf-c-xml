﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Globalization;
using System.Windows.Media.Animation;



namespace DibujandoFormas
{

    public class MoldeRecta : FiguraAbstracta
    {
        public MoldeRecta()
        {
            CalculateGeometry();
        }

        public new String name
        {

            get { return "Recta         "; }
        }

        public override string ToString()
        {

            return "Recta";
        }


        public override void CalculateGeometry()
        {
            
            DrawingContext dc = RenderOpen();

            Point p1 = new Point();
            Point p2 = new Point();

            p1 = propOrigen;
            p2 = propfin;
            posicion.X = 0.00;
            posicion.Y = 0.00;
            //  dc.DrawLine(penAbstracto,linea);

            dc.DrawLine(penAbstracto, p1, p2);
            dc.Close();
            base.CalculateGeometry();

            

        
       


        }

        public override void DrawContour(DrawingContext dc, Pen pen, double xoffset = 0, double yoffset = 0, double rot = 0, double scalex = 1, double scaley = 1)
        {


        /*    Point pt = new Point(posicion.X + xoffset, posicion.Y + yoffset);
            RotateTransform rtf = new RotateTransform(anguloAbstracto + rot, pt.X, pt.Y);
            dc.PushTransform(rtf);
            dc.DrawEllipse(null, pen, pt, anchoAbstracto / 2 * scalex, altoAbstracto / 2 * scaley);
            dc.DrawLine(pen, , clickEndPoint);
            
            dc.Pop();
            */


            Point pt = new Point(posicion.X + xoffset, posicion.Y + yoffset);
            ScaleTransform stf = new ScaleTransform(scalex, scaley, pt.X, pt.Y);
            dc.PushTransform(stf);
            RotateTransform rtf = new RotateTransform(anguloAbstracto + rot, pt.X, pt.Y);
            dc.PushTransform(rtf);
            TranslateTransform ttf = new TranslateTransform(pt.X, pt.Y);
            dc.PushTransform(ttf);

            dc.DrawGeometry(null, pen, bezierPath.Data);
            dc.Pop();
            dc.Pop();
            dc.Pop();
        }


    }

}