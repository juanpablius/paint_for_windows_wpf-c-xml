﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DibujandoFormas
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public bool csChecked = false;
        private bool isChecked = true;
        public Opciones ventanaDeOpciones;
        public Contenedor XAMLlienzo
        {
            get { return mixamllienzo; }
        }
        public Controladora XAMLcontroladora
        {
            get { return mixamlcontroladora; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public MainWindow()
        {
            ventanaDeOpciones = null;
            InitializeComponent();
            mixamlcontroladora._lienzoRef = mixamllienzo;


            mixamllienzo.timer.Start();
        }


        protected override void OnClosing(CancelEventArgs e)
        {
            if (ventanaDeOpciones != null) ventanaDeOpciones.Close();
        }

        private void OnResizedWindow(object sender, SizeChangedEventArgs e)
        {
            mixamllienzo.OnWindowResized();
            mixamlcontroladora.OnWindowResized();
        }

        private void btnPropertiesClick(object sender, RoutedEventArgs e)
        {
            if (ventanaDeOpciones == null)
            {
                ventanaDeOpciones = new Opciones();
                ventanaDeOpciones.DataContext = mixamlcontroladora;
                ventanaDeOpciones.dgShapes.DataContext = this;
                ventanaDeOpciones.Show();

            }
            else
            {
                CloseProperties();
            }
        }

        public void CloseProperties()
        {
            ventanaDeOpciones.Close();
            ventanaDeOpciones = null;
        }



    
        private void radioDibujandoF(object sender, RoutedEventArgs e)
        {
            //nonada
           // cmbShape.Visibility = Visibility.Visible;
        }

        private void radioSelec(object sender, RoutedEventArgs e)
        {
            cmbShape.Visibility = Visibility.Hidden;
        }

        private void radioClickDibFig(object sender, RoutedEventArgs e)
        {
            cmbShape.Visibility = Visibility.Visible;
        }


        public String propLabelSI
        {
          
            set { labelSI.Content = value; }
        }


        


        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void cuadriculaSuperior(object sender, RoutedEventArgs e)
        {
           
        }

        private void PosicionRaton(object sender, MouseEventArgs e)
        {
            Point posMouse = e.GetPosition(this);
            String oh3 = String.Format("{0} x {1}", posMouse.X - 10, posMouse.Y - 60);
            cordX.Content = oh3;
            
           
        }
    }
}
