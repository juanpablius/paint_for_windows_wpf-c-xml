﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Globalization;
using System.Windows.Media.Animation;

namespace DibujandoFormas
{
    /// <summary>
    /// Lógica de interacción para Opciones.xaml
    /// </summary>
    public partial class Opciones : Window
    {
        public Opciones()
        {
            InitializeComponent();
        }

        protected override void OnClosed(EventArgs e)
        {
            ((MainWindow)(Application.Current.MainWindow)).CloseProperties();
        }

        private void btnRemoveClick(object sender, RoutedEventArgs e)
        {
            ((Controladora)DataContext).RemoveShape();

        }

        private void btnAddClick(object sender, RoutedEventArgs e)
        {
        

            FiguraAbstracta rshape = ((Controladora)DataContext).randomShape;

            FiguraAbstracta shape = FiguraAbstracta.CreateShape((figurasEnnumeradas)cmbShape.SelectedItem,
                new Vector(rshape.pX, rshape.pY),
                new Point(rshape.propOrigen.X, rshape.propOrigen.Y),
                new Point(rshape.propFinX, rshape.propFinY),
                rshape.ancho, rshape.alto, rshape.valorAngulo, rshape.rellenoDeBrocha, rshape.valorPen);

            ((MainWindow)Application.Current.MainWindow).mixamllienzo.agregaFigura(shape);
            ((Controladora)DataContext).randomShape.normalizar() ;
            // Figura nueva


        }


        private void cmbShapeSelectionChanged(object sender, SelectionChangedEventArgs e)
        {



            switch ((figurasEnnumeradas)cmbShape.SelectedItem)
            {
                case figurasEnnumeradas.Rectangulo:
                case figurasEnnumeradas.Elipse:
                case figurasEnnumeradas.Triangulo:
                case figurasEnnumeradas.RectanguloRed:
                case figurasEnnumeradas.Circulo:
                case figurasEnnumeradas.Rombo:
                    lblSize.Content = "Ancho";
                    lblH.Content = "Alto";
                    box2.Header = "Dimensiones";
                    txtX.Visibility = Visibility.Visible;
                    txtY.Visibility = Visibility.Visible;
                    txtH.Visibility = Visibility.Visible;
                    txtW.Visibility = Visibility.Visible;
                    lblH.Visibility = Visibility.Visible;
                    origenXbox.Visibility = Visibility.Hidden;



                    txtH.Visibility = Visibility.Visible;
                    lblH.Visibility = Visibility.Visible;




                    break;
                case figurasEnnumeradas.Recta:
                    txtX.Visibility = Visibility.Collapsed;
                    txtY.Visibility = Visibility.Collapsed;


                    txtW.Visibility = Visibility.Collapsed;
                    txtH.Visibility = Visibility.Collapsed;
                    box2.Header = "Destino";
               
                    lblSize.Content = "X";
                    lblH.Content = "Y";

                    origenXbox.Visibility = Visibility.Visible;
                    origenYBox.Visibility = Visibility.Visible;

                    destinoXbox.Visibility = Visibility.Visible;
                    destinoYBox.Visibility = Visibility.Visible;



                    
                    break;
            }
        }

        private void TextGotFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            



        }

        private void TextLostFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            ((Controladora)DataContext).InvalidateVisual();

        }

        private void TextLostFocus2(object sender, RoutedEventArgs e)
        {

        }

        private void dgShapes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Debug.Write("Mario Conde");
            BotonHecho.Visibility = Visibility.Visible;
            BotonVolver.Visibility = Visibility.Visible;
            botonEliminar.Visibility = Visibility.Visible;
            BotonAgregar.Visibility = Visibility.Collapsed;
            cmbShape.Visibility = Visibility.Collapsed;
            labelidSeleccionado.Visibility = Visibility.Visible;
            labelnombreSeleccionado.Visibility = Visibility.Visible;

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void clickHecho(object sender, RoutedEventArgs e)
        {
            ((Controladora)DataContext).InvalidateVisual();
        }

        private void VolverAInicio(object sender, RoutedEventArgs e)
        {
            ((Controladora)DataContext).Seleccionar();
            botonEliminar.Visibility = Visibility.Collapsed;
            BotonAgregar.Visibility = Visibility.Visible;
            BotonVolver.Visibility = Visibility.Collapsed;
            BotonHecho.Visibility = Visibility.Collapsed;
            cmbShape.Visibility = Visibility.Visible;
            labelidSeleccionado.Visibility = Visibility.Collapsed;
            labelnombreSeleccionado.Visibility = Visibility.Collapsed;
        }

        private void clickLimpiarLienzo(object sender, RoutedEventArgs e)
        {
           
            if (MessageBox.Show("¿Está seguro de querer limpiar el lienzo?","Se borrarán todas las figuras creadas", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                ((Controladora)DataContext).OrdenaLimpiarLienzo();
                MessageBox.Show("Figuras borradas del lienzo");
            }
           
          
        }






    }
    

    public class BoolToValueConverter<T> : IValueConverter
    {
        public T FalseValue { get; set; }
        public T TrueValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return FalseValue;
            else
                return (bool)value ? TrueValue : FalseValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value != null ? value.Equals(TrueValue) : false;
        }
    }

    public class BooleanToStringConverter : BoolToValueConverter<String>
    {
        public BooleanToStringConverter()
        {
            FalseValue = "Run";
            TrueValue = "Stop";
        }
    }

    public enum botonesEnnumerados
    {
        Tool_Cursor,
        Tool_Pencil,
        Goma,
        Desplaza,
        Modifica
      
    }

   // public 

    public enum figurasEnnumeradas
    {
       
        Recta = 0,
        Elipse = 1,
        Rectangulo = 2,
        Triangulo = 3,
        RectanguloRed = 4,
        Circulo = 5,
        Rombo = 6
        
    }

    public class EnumMatchToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //Debug.WriteLine("Called Convert value{0}, parameter{1}", value, parameter);
            return value.Equals(parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //Debug.WriteLine("Called ConvertBack value{0}, parameter{1}", value, parameter);
            return value.Equals(true) ? parameter : Binding.DoNothing;
        }
    }

    public class BooleanToStringConverter2 : BoolToValueConverter<String>
    {
        public BooleanToStringConverter2()
        {

            FalseValue = "Añadir";
            TrueValue = "Hecho";
        }
    }
}
